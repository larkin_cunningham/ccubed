package io.larkin.ccubed.app;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by larkin on 14/05/14.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ElementText {

    @JsonProperty("html")
    private Boolean isHtml;

    @JsonProperty("text")
    private String text;

    @JsonProperty("element")
    private Element element;

    public Element getElement() {
        return element;
    }

    public void setElement(Element element) {
        this.element = element;
    }

    public boolean isHtml() {
        return isHtml;
    }

    public void setHtml(boolean html) {
        this.isHtml = html;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
