package io.larkin.ccubed.app;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by larkin on 14/05/14.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Files {

    @JsonProperty("count")
    private Integer count;

    @JsonProperty("url")
    private String url;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}
