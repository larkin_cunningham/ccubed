package io.larkin.ccubed.app;

import java.util.Date;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by larkin on 12/05/14.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Item {

    @JsonProperty("id")
    private Integer id;

    @JsonProperty("url")
    private String url;

    @JsonProperty("added")
    private Date added;

    @JsonProperty("modified")
    private Date modified;

    @JsonProperty("item_type")
    Element itemType;

    @JsonProperty("collection")
    Element collection;

    @JsonProperty("owner")
    Element owner;

    @JsonProperty("files")
    Files files;

    @JsonProperty("tags")
    List<Element> tags;

    @JsonProperty("element_texts")
    List<ElementText> elementTexts;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Date getAdded() {
        return added;
    }

    public void setAdded(Date added) {
        this.added = added;
    }

    public Date getModified() {
        return modified;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    public Element getItemType() {
        return itemType;
    }

    public void setItemType(Element itemType) {
        this.itemType = itemType;
    }

    public Element getCollection() {
        return collection;
    }

    public void setCollection(Element collection) {
        this.collection = collection;
    }

    public Element getOwner() {
        return owner;
    }

    public void setOwner(Element owner) {
        this.owner = owner;
    }

    public Files getFiles() {
        return files;
    }

    public void setFiles(Files files) {
        this.files = files;
    }

    public List<Element> getTags() {
        return tags;
    }

    public void setTags(List<Element> tags) {
        this.tags = tags;
    }

    public List<ElementText> getElementTexts() {
        return elementTexts;
    }

    public void setElementTexts(List<ElementText> elementTexts) {
        this.elementTexts = elementTexts;
    }
}
